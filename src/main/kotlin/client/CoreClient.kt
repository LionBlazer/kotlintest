package client

import java.io.ByteArrayOutputStream
import java.io.DataOutputStream
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress

object CoreClient {
    private val socket = DatagramSocket()

    fun send(id: String) {
        val baos = ByteArrayOutputStream(1024)
        baos.use {
            val dos = DataOutputStream(it)
            val array = id.toByteArray()
            dos.writeInt(array.size)
            dos.write(array)
        }
        val buf = baos.toByteArray()


        val packet = DatagramPacket(buf, buf.size, InetAddress.getLocalHost(), 4485)
        socket.send(packet)
    }
}


fun main() {
    CoreClient.send("test")
    CoreClient.send("ky")
    CoreClient.send("exit")
}