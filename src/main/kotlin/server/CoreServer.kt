package server

import kotlinx.coroutines.*
import java.io.ByteArrayInputStream
import java.io.DataInputStream
import java.net.DatagramPacket
import java.net.DatagramSocket
import kotlin.system.exitProcess

object CoreServer : Thread() {
    private val socket = DatagramSocket(4485)

    private lateinit var buf : ByteArray

    override fun run() {
        while (true) {
            buf = ByteArray(1024)
            val packet = DatagramPacket(buf, buf.size)
            socket.receive(packet)
            handleClient(packet)
        }
    }


    private fun handleClient(datagramPacket: DatagramPacket){
        val data = datagramPacket.data
        DataInputStream(ByteArrayInputStream(data)).use {
            val array = ByteArray(it.readInt())
            it.read(array)
            when(val read = String(array)){
                "exit" -> exitProcess(0)
                else -> println(read)
            }
        }
    }
}

fun main() {
    println("Start")
    CoreServer.start()
}
